 <div class="pluto-theme-container pluto-theme-padding-tb-default">
        <section class="pluto-theme-post-layout-ten d-flex flex-sm-column">
            <div class="pluto-theme-with-two-post pluto-theme-two-post-on-left w-30 d-flex flex-column justify-content-between w-sm-100">
                <div class="pluto-theme-post ">
                <?php while ( $post_query_first->have_posts() ) : $post_query_first->the_post();
						$post_id = get_the_ID();
						$terms = get_the_terms( $post_id, 'category' ); 
					?>
                    <?php
						$widget_1_small = wp_get_attachment_image_src(get_post_thumbnail_id(), 'pluto-737-983');
						// $widget_3_big_mobile = wp_get_attachment_image_src(get_post_thumbnail_id(), 'pluto-450-600');
    
					if ($widget_1_small) { ?>
                    <div class="pluto-theme-logo-img">
                    <img src="<?php echo esc_url($widget_1_small[0]); ?>" alt="<?php the_title_attribute(); ?>" class="space-desktop-view">
                    </div>
                    <div class="pluto-theme-title">
                        <a class="h3 f-s-6 f-w-bold" href="#"><?php get_the_title() ? the_title() : the_ID(); ?></a>
                    </div>
                    <div class="pluto-theme-post-info">
                        <ul class="d-flex justify-content-start">
                        <li class="d-flex align-items-center f-s-4">
                        <div class="pluto-theme-icon">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/visibility-grey.svg" alt="#" width="25" height="25">
                        </div>
                        <span>485</span>
                    </li>
                    <li class="d-flex align-items-center f-s-4">
                        <div class="pluto-theme-icon">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/message-grey.svg" alt="#" width="25" height="25">
                        </div>
                        <span><?php comments_number( '0', '1', '%' ); ?></span>
                    </li>
                    <li class="d-flex align-items-center f-s-4">
                        <div class="pluto-theme-icon">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/timer-grey.svg" alt="#" width="25" height="25">
                        </div>
                        <span><?php printf( esc_html_x( '%s ago', '%s = human-readable time difference', 'spacethemes' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
                    </li>
                        </ul>
                    </div>
                    
                </div>
                <div class="pluto-theme-post">
                    <div class="pluto-theme-logo-img">
                    <img src="<?php echo esc_url($widget_1_small[0]); ?>" alt="<?php the_title_attribute(); ?>" class="space-desktop-view">
                    </div>
                    <div class="pluto-theme-title">
                        <a class="h3 f-s-6 f-w-bold" href="#"><?php get_the_title() ? the_title() : the_ID(); ?></a>
                    </div>
                    <div class="pluto-theme-post-info">
                        <ul class="d-flex justify-content-start">
                        <li class="d-flex align-items-center f-s-4">
                        <div class="pluto-theme-icon">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/visibility-grey.svg" alt="#" width="25" height="25">
                        </div>
                        <span>485</span>
                    </li>
                    <li class="d-flex align-items-center f-s-4">
                        <div class="pluto-theme-icon">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/message-grey.svg" alt="#" width="25" height="25">
                        </div>
                        <span><?php comments_number( '0', '1', '%' ); ?></span>
                    </li>
                    <li class="d-flex align-items-center f-s-4">
                        <div class="pluto-theme-icon">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/timer-grey.svg" alt="#" width="25" height="25">
                        </div>
                        <span><?php printf( esc_html_x( '%s ago', '%s = human-readable time difference', 'spacethemes' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
                    </li>
                        </ul>
                    </div>
                </div>
                <?php } ?>
					<?php
						endwhile;
						wp_reset_postdata();
					?>
            </div>
            <div class="pluto-theme-with-one-post w-50 w-sm-100">
                <div class="pluto-theme-post d-flex flex-column justify-content-between">
                <?php 
							$post_query_second = new WP_Query( $second_args );
							while ( $post_query_second->have_posts() ) : $post_query_second->the_post();
                            $post_id = get_the_ID();
                            $terms = get_the_terms( $post_id, 'category' );
						?>
                         <?php $widget_2_small = wp_get_attachment_image_src(get_post_thumbnail_id(), 'pluto-450-450'); if ($widget_2_small) { ?>
                    <div class="pluto-theme-logo-img">
                    <img src="<?php echo esc_url($widget_2_small[0]); ?>" alt="<?php the_title_attribute(); ?>">
                    </div>
                    <div class="pluto-theme-title">
                        <span class="h3 f-s-11 f-w-bold f-s-sm-6"><?php get_the_title() ? the_title() : the_ID(); ?> </span>
                        <p class="pluto-theme-read-content f-s-5 f-w-regular"><?php echo get_the_content(); ?></p>
                        <a class="read-more f-s-5 f-w-medium f-s-6" href="#">Read More</a>
                    </div>
                    <div class="pluto-theme-post-info">
                        <ul class="d-flex justify-content-start" >
                        <li class="d-flex align-items-center f-s-4">
                        <div class="pluto-theme-icon">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/visibility-grey.svg" alt="#" width="25" height="25">
                        </div>
                        <span>485</span>
                    </li>
                    <li class="d-flex align-items-center f-s-4">
                        <div class="pluto-theme-icon">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/message-grey.svg" alt="#" width="25" height="25">
                        </div>
                        <span><?php comments_number( '0', '1', '%' ); ?></span>
                    </li>
                    <li class="d-flex align-items-center f-s-4">
                        <div class="pluto-theme-icon">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/timer-grey.svg" alt="#" width="25" height="25">
                        </div>
                        <span><?php printf( esc_html_x( '%s ago', '%s = human-readable time difference', 'spacethemes' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
                    </li>
                        </ul>
                    </div>
                </div>
                <?php } ?>
                <?php endwhile; ?>
            </div>
            <div class="pluto-theme-with-two-post pluto-theme-two-post-on-right w-25 d-flex flex-column justify-content-between w-sm-100">
                <div class="pluto-theme-post">
                    <div class="pluto-theme-logo-img">
                    <img src="<?php echo esc_url($widget_1_small[0]); ?>" alt="<?php the_title_attribute(); ?>" class="space-desktop-view">
                    </div>
                    <div class="pluto-theme-title">
                        <a class="h3 f-s-6 f-w-bold" href="#"><?php get_the_title() ? the_title() : the_ID(); ?></a>
                    </div>
                    <div class="pluto-theme-post-info">
                        <ul class="d-flex justify-content-start">
                        <li class="d-flex align-items-center f-s-4">
                        <div class="pluto-theme-icon">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/visibility-grey.svg" alt="#" width="25" height="25">
                        </div>
                        <span>485</span>
                    </li>
                    <li class="d-flex align-items-center f-s-4">
                        <div class="pluto-theme-icon">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/message-grey.svg" alt="#" width="25" height="25">
                        </div>
                        <span><?php comments_number( '0', '1', '%' ); ?></span>
                    </li>
                    <li class="d-flex align-items-center f-s-4">
                        <div class="pluto-theme-icon">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/timer-grey.svg" alt="#" width="25" height="25">
                        </div>
                        <span><?php printf( esc_html_x( '%s ago', '%s = human-readable time difference', 'spacethemes' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
                    </li>
                        </ul>
                    </div>
                </div>
                <div class="pluto-theme-post">
                    <div class="pluto-theme-logo-img">
                    <img src="<?php echo esc_url($widget_1_small[0]); ?>" alt="<?php the_title_attribute(); ?>" class="space-desktop-view">
                    </div>
                    <div class="pluto-theme-title">
                        <a class="h3 f-s-6 f-w-bold" href="#"><?php get_the_title() ? the_title() : the_ID(); ?></a>
                    </div>
                    <div class="pluto-theme-post-info">
                    <ul class="d-flex justify-content-start">
                        <li class="d-flex align-items-center f-s-4">
                        <div class="pluto-theme-icon">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/visibility-grey.svg" alt="#" width="25" height="25">
                        </div>
                        <span>485</span>
                    </li>
                    <li class="d-flex align-items-center f-s-4">
                        <div class="pluto-theme-icon">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/message-grey.svg" alt="#" width="25" height="25">
                        </div>
                        <span><?php comments_number( '0', '1', '%' ); ?></span>
                    </li>
                    <li class="d-flex align-items-center f-s-4">
                        <div class="pluto-theme-icon">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/timer-grey.svg" alt="#" width="25" height="25">
                        </div>
                        <span><?php printf( esc_html_x( '%s ago', '%s = human-readable time difference', 'spacethemes' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
                    </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>