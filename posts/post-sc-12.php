<section class="pluto-theme-post-layout-twelve">
    <div class="pluto-theme-container pluto-theme-padding-tb-default">
        <div class="post-layout-twelve-list-box">
            <div class="pluto-theme-row d-flex flex-md-column">
                <div class="pluto-theme-column-one w-45 w-md-100">
                <?php while ( $post_query_first->have_posts() ) : $post_query_first->the_post();
						$post_id = get_the_ID();
						$terms = get_the_terms( $post_id, 'category' );
					?> 
                    <?php
						$widget_1_big = wp_get_attachment_image_src(get_post_thumbnail_id(), 'pluto-737-983');
						// $widget_3_big_mobile = wp_get_attachment_image_src(get_post_thumbnail_id(), 'pluto-450-600');
    
					if ($widget_1_big) { ?>
                    <div class="pluto-theme-image">
                        <a href="#">
                        <img src="<?php echo esc_url($widget_1_big[0]); ?>" alt="<?php the_title_attribute(); ?>" class="space-desktop-view">
                        </a>
                        <div class="overlay-shadow"></div>
                    </div>
                    <div class="post-content">
                        <ul class="d-flex ">
                            <li class="f-w-medium f-s-3"> <?php echo get_the_date(); ?></li>
                            <li> <?php echo get_the_author(); ?></li>
                        </ul>
                        <div class="post-title">
                            <a class="h3 f-s-9 f-w-bold f-s-xxsm-5" href="#"> <?php get_the_title() ? the_title() : the_ID(); ?></a>
                        </div>
                        <span class="viewed d-flex align-items-center f-w-normal f-s-3">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/visibility-white.svg" alt="pluto-theme-image">984 VIEWS</span>
                    </div>
                    <?php } ?>
					<?php
						endwhile;
						wp_reset_postdata();
					?>
                </div>
                <div class="pluto-theme-column-two d-flex flex-wrap w-20 w-md-100 justify-content-between">
                <?php 
							$post_query_second = new WP_Query( $second_args );
							while ( $post_query_second->have_posts() ) : $post_query_second->the_post();
                            $post_id = get_the_ID();
                            $terms = get_the_terms( $post_id, 'category' );
						?>
                        <?php $widget_2_small = wp_get_attachment_image_src(get_post_thumbnail_id(), 'pluto-450-450'); if ($widget_2_small) { ?>
                    <div class="pluto-theme-row w-100 d-md-flex ">
                        <div class="pluto-theme-image">
                            <a href="#"> <img src="<?php echo esc_url($widget_2_small[0]); ?>" alt="<?php the_title_attribute(); ?>"></a>
                            <div class="overlay-shadow w-100"></div>
                        </div>
                        <div class="post-content">
                            <ul class="d-flex">
                                <li class="f-w-medium f-s-3"><?php echo get_the_date(); ?></li>
                                <li class="f-w-medium f-s-3"><?php echo get_the_author(); ?></li>
                            </ul>
                            <div class="post-title">
                                <a class="h3 f-s-5 f-w-bold" href="#"> <?php get_the_title() ? the_title() : the_ID(); ?></a>
                            </div>
                            <span class="viewed d-flex align-items-center f-s-3 f-w-normal">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/visibility-white.svg" alt="pluto-theme-image">984 VIEWS</span>
                        </div>
                    </div>
                    <div class="pluto-theme-row w-100 d-md-flex ">
                        <div class="pluto-theme-image">
                            <a href="#"><img src="<?php echo esc_url($widget_2_small[0]); ?>" alt="<?php the_title_attribute(); ?>"></a>
                            <div class="overlay-shadow"></div>
                        </div>
                        <div class="post-content">
                            <ul class="d-flex">
                                <li class="f-w-medium f-s-3"><?php echo get_the_date(); ?></li>
                                <li class="f-w-medium f-s-3"><?php echo get_the_author(); ?></li>
                            </ul>
                            <div class="post-title">
                                <a class="h3 f-s-5 f-w-bold" href="#"> <?php get_the_title() ? the_title() : the_ID(); ?></a>
                            </div>
                            <span class="viewed d-flex align-items-center f-s-3 f-w-normal">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/visibility-white.svg" alt="pluto-theme-image">984 VIEWS</span>
                        </div>
                    </div>
                    <?php } ?>
                            <?php endwhile; ?>
                </div>
                <div class="pluto-theme-column-three w-35 d-flex flex-wrap align-content-between w-md-100">
                    <div class="post-content">
                        <ul class="d-flex">
                            <li class="f-s-3 f-w-medium"><?php echo get_the_date(); ?></li>
                            <li  class="f-s-3 f-w-medium"><?php echo get_the_author(); ?></li>
                        </ul>
                        <div class="post-title">
                            <a class="h3  f-s-5 f-w-bold" href="#"> <?php get_the_title() ? the_title() : the_ID(); ?></a>
                        </div>
                        <span class="viewed d-flex align-items-center f-w-normal f-s-3">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/visibility-grey.svg" alt="pluto-theme-image">984 VIEWS</span>
                    </div>
                    <div class="post-content">
                        <ul class="d-flex">
                            <li  class="f-s-3 f-w-medium"><?php echo get_the_date(); ?></li>
                            <li class="f-s-3 f-w-medium"><?php echo get_the_author(); ?></li>
                        </ul>
                        <div class="post-title">
                            <a class="h3 f-w-bold f-s-5" href="#"> <?php get_the_title() ? the_title() : the_ID(); ?></a>
                        </div>
                        <span class="viewed d-flex align-items-center f-w-normal f-s-3">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/visibility-grey.svg" alt="pluto-theme-image">984 VIEWS</span>
                    </div>
                    <div class="post-content">
                        <ul class="d-flex">
                            <li class="f-s-3 f-w-medium"><?php echo get_the_date(); ?></li>
                            <li class="f-s-3 f-w-medium"><?php echo get_the_author(); ?></li>
                        </ul>
                        <div class="post-title">
                            <a class="h3 f-w-bold f-s-5" href="#"> <?php get_the_title() ? the_title() : the_ID(); ?></a>
                        </div>
                        <span class="viewed d-flex align-items-center f-w-normal f-s-3">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/visibility-grey.svg" alt="pluto-theme-image">984 VIEWS</span>
                    </div>
                    <div class="post-content">
                        <ul class="d-flex">
                            <li class="f-s-3 f-w-medium"><?php echo get_the_date(); ?></li>
                            <li class="f-s-3 f-w-medium"><?php echo get_the_author(); ?></li>
                        </ul>
                        <div class="post-title">
                            <a class="h3 f-w-bold f-s-5" href="#"> <?php get_the_title() ? the_title() : the_ID(); ?></a>
                        </div>
                        <span class="viewed d-flex align-items-center f-w-normal f-s-3">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/visibility-grey.svg" alt="pluto-theme-image">984 VIEWS</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>