<section class="pluto-theme-post-layout-seven">
        <div class="post-layout-seven-list-box w-100 d-flex flex-wrap justify-content-between">
            <div class="column w-50 d-flex w-md-100 w-xxsm-100">
            <?php while ( $post_query_first->have_posts() ) : $post_query_first->the_post();
						$post_id = get_the_ID(); 
						$terms = get_the_terms( $post_id, 'category' );
					?>
                    <?php
						$widget_1_big = wp_get_attachment_image_src(get_post_thumbnail_id(), 'pluto-737-983');
                        if ($widget_1_big) { ?>
                <div class="pluto-theme-image w-50 d-flex justify-content-center">
                    <a href="#">
                    <img src="<?php echo esc_url($widget_1_big[0]); ?>" alt="<?php the_title_attribute(); ?>" class="space-desktop-view">
                    </a>
                    <span class="post-category f-s-4 f-w-regular d-flex justify-content-center"><?php the_category(' '); ?></span>
                </div>
                <div class="bonus w-100">
                    <a class="h3 f-s-10 f-w-medium f-xxsm-5" href="#"><?php the_title_attribute(); ?></a>
                    <ul class="d-flex align-items-center">
                    <li class="d-flex align-items-center f-w-regular f-s-5 f-xxsm-3"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/timer-grey.svg" alt="pluto-theme-image"><?php echo get_the_date(); ?>
                    </li>
                    <li class="d-flex align-items-center f-w-regular f-s-5 f-xxsm-3"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/visibility-grey.svg" alt="pluto-theme-image">541 views
                    </li>
                    </ul>
                </div>
            </div>

            <div class="column w-50 d-flex w-md-100 w-xxsm-100">
                <div class="pluto-theme-image w-50 d-flex justify-content-center">
                    <a href="#">
                    <img src="<?php echo esc_url($widget_1_big[0]); ?>" alt="<?php the_title_attribute(); ?>" class="space-desktop-view">
                    </a>
                    <span class="post-category f-s-4 f-w-regular d-flex justify-content-center"><?php the_category(' '); ?></span>
                </div>
                <div class="bonus w-100">
                    <a class="h3 f-s-10 f-w-medium f-xxsm-5" href="#"><?php the_title_attribute(); ?></a>
                    <ul class="d-flex align-items-center">
                    <li class="d-flex align-items-center f-w-regular f-s-5 f-xxsm-3"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/timer-grey.svg" alt="pluto-theme-image"><?php echo get_the_date(); ?>
                    </li>
                    <li class="d-flex align-items-center f-w-regular f-s-5 f-xxsm-3"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/visibility-grey.svg" alt="pluto-theme-image">541 views
                    </li>
                    </ul>
                </div>
            </div>

            <div class="column w-50 d-flex w-md-100 w-xxsm-100">
                <div class="pluto-theme-image w-50 d-flex justify-content-center">
                    <a href="#">
                    <img src="<?php echo esc_url($widget_1_big[0]); ?>" alt="<?php the_title_attribute(); ?>" class="space-desktop-view">
                    </a>
                    <span class="post-category f-s-4 f-w-regular d-flex justify-content-center"><?php the_category(' '); ?></span>
                </div>
                <div class="bonus w-100">
                    <a class="h3 f-s-10 f-w-medium f-xxsm-5" href="#"><?php the_title_attribute(); ?></a>
                    <ul class="d-flex align-items-center">
                    <li class="d-flex align-items-center f-w-regular f-s-5 f-xxsm-3"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/timer-grey.svg" alt="pluto-theme-image"><?php echo get_the_date(); ?>
                    </li>
                    <li class="d-flex align-items-center f-w-regular f-s-5 f-xxsm-3"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/visibility-grey.svg" alt="pluto-theme-image">541 views
                    </li>
                    </ul>
                </div>
            </div>

            <div class="column w-50 d-flex w-md-100 w-xxsm-100">
                <div class="pluto-theme-image w-50 d-flex justify-content-center">
                    <a href="#">
                    <img src="<?php echo esc_url($widget_1_big[0]); ?>" alt="<?php the_title_attribute(); ?>" class="space-desktop-view">
                    </a>
                    <span class="post-category f-s-4 f-w-regular d-flex justify-content-center"><?php the_category(' '); ?></span>
                </div>
                <div class="bonus w-100">
                    <a class="h3 f-s-10 f-w-medium f-xxsm-5" href="#"><?php the_title_attribute(); ?></a>
                    <ul class="d-flex align-items-center">
                    <li class="d-flex align-items-center f-w-regular f-s-5 f-xxsm-3"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/timer-grey.svg" alt="pluto-theme-image"><?php echo get_the_date(); ?>
                    </li>
                    <li class="d-flex align-items-center f-w-regular f-s-5 f-xxsm-3"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/visibility-grey.svg" alt="pluto-theme-image">541 views
                    </li>
                    </ul>
                </div>
                <?php } ?>
                <?php
						endwhile;
						wp_reset_postdata();
					?> 
            </div>     
        </div>
    </section>