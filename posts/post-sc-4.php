<section class="pluto-theme-post-layout-four">
    <div class="post-layout-four-list-box">
        <div class="pluto-theme-top-row">
            <div class="game-card"> 
                <div class="pluto-theme-row-one d-flex w-md-100 flex-md-wrap">
                    <div class="column-one w-50 w-md-100">
                    <?php while ( $post_query_first->have_posts() ) : $post_query_first->the_post();
						$post_id = get_the_ID();
						$terms = get_the_terms( $post_id, 'category' );
					?>
                    <?php
						$widget_1_small = wp_get_attachment_image_src(get_post_thumbnail_id(), 'pluto-737-983');
						// $widget_3_big_mobile = wp_get_attachment_image_src(get_post_thumbnail_id(), 'pluto-450-600');
    
					if ($widget_1_small) { ?>
                        <div class="pluto-theme-image w-100 w-md-100">
                        <img src="<?php echo esc_url($widget_1_small[0]); ?>" alt="<?php the_title_attribute(); ?>" class="space-desktop-view">
                        </div>
                        <div class="post-title">
                            <a class="h3 f-s-10 f-w-medium" href="#"><?php get_the_title() ? the_title() : the_ID(); ?></h3></a>
                        </div>
                        <span class="publish-date f-s-6 f-w-regular"><?php echo get_the_date(); ?></span>
                        <?php } ?>
					<?php
						endwhile;
						wp_reset_postdata();
					?>
                    </div>

                    <div class="column-two w-50 d-flex flex-wrap w-md-100 w-sm-100">
                    <?php 
							$post_query_second = new WP_Query( $second_args );
							while ( $post_query_second->have_posts() ) : $post_query_second->the_post();
                            $post_id = get_the_ID();
                            $terms = get_the_terms( $post_id, 'category' );
						?>
                         <?php $widget_2_small = wp_get_attachment_image_src(get_post_thumbnail_id(), 'pluto-450-450'); if ($widget_2_small) { ?>
                        <div class="row-one d-flex w-md-100 w-xsm-100 flex-sm-column">
                      
                            <div class="pluto-theme-image w-50 w-sm-100">
                                <a href="#"> <img src="<?php echo esc_url($widget_2_small[0]); ?>" alt="<?php the_title_attribute(); ?>"></a>
                            </div>
                            <div class="bonus w-50 w-sm-100">
                                <div class="post-title">
                                    <a class="h3 f-s-7 f-w-medium" href="#"><?php get_the_title() ? the_title() : the_ID(); ?></h3></a>
                                </div>
                                <span class="publish-date f-s-4 f-w-regular"><?php echo get_the_date(); ?></span>
                            </div>
                        </div>
                        <div class="row-one d-flex d-flex w-md-100 w-xsm-100 flex-sm-column">
                            <div class="pluto-theme-image w-50 w-sm-100">
                                <a href="#"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/posts/dota.png" alt="pluto-theme-image"></a>
                            </div>
                            <div class="bonus w-50 w-sm-100">
                                <div class="post-title">
                                    <a class="h3 f-s-7 f-w-medium" href="#"><?php get_the_title() ? the_title() : the_ID(); ?></h3></a>
                                </div>
                                <span class="publish-date f-s-4 f-w-regular"><?php echo get_the_date(); ?></span>
                            </div>
                        </div>
                        <?php } ?>
                <?php endwhile; ?>
                    </div>
                </div>
            </div>
            <div class="pluto-theme-row-two d-flex flex-wrap justify-content-between">
       
                <div class="card w-33 w-md-100">
              
                    <article class="column d-md-flex flex-md-wrap">
                        <div class="pluto-theme-image w-md-100">
                        <img src="<?php echo esc_url($widget_1_small[0]); ?>" alt="<?php the_title_attribute(); ?>" class="space-desktop-view">
                        </div>
                        <div class="bonus">
                            <div class="post-title">
                                <a class="h3 f-s-7 f-w-medium" href="#"><?php get_the_title() ? the_title() : the_ID(); ?></h3>
                                </a>
                            </div>
                            <span class="publish-date d-flex f-s-4 f-w-regular"><?php echo get_the_date(); ?></span>
                        </div>
                    </article>
                  
                   
                </div>
                <div class="card w-33 w-md-100">
                    <article class="column d-md-flex flex-md-wrap">
                        <div class="pluto-theme-image w-md-100">
                        <img src="<?php echo esc_url($widget_1_small[0]); ?>" alt="<?php the_title_attribute(); ?>" class="space-desktop-view">
                        </div>
                        <div class="bonus">
                            <div class="post-title">
                                <a class="h3 f-s-7 f-w-medium" href="#"><?php get_the_title() ? the_title() : the_ID(); ?></h3>
                                </a>
                            </div>
                            <span class="publish-date d-flex f-s-4 f-w-regular"><?php echo get_the_date(); ?></span>
                        </div>
                    </article>
                </div>
                <div class="card w-33 w-md-100">
                    <article class="column d-md-flex flex-md-wrap">
                        <div class="pluto-theme-image w-md-100">
                        <img src="<?php echo esc_url($widget_1_small[0]); ?>" alt="<?php the_title_attribute(); ?>" class="space-desktop-view">
                        </div>
                        <div class="bonus">
                            <div class="post-title">
                                <a class="h3 f-s-7 f-w-medium" href="#"><?php get_the_title() ? the_title() : the_ID(); ?></h3>
                                </a>
                            </div>
                            <span class="publish-date d-flex f-s-4 f-w-regular"><?php echo get_the_date(); ?></span>
                        </div>
                    </article>
                </div>
           
            </div>
        </div>
    </div>
</section>