<div class="pluto-theme-container pluto-theme-padding-tb-default">
    <section class="pluto-theme-post-layout-eleven">
        <div class="post-layout-eleven-list-box">
            <div class="pluto-theme-single-column d-flex align-items-center flex-sm-column">
            <?php while ( $post_query_first->have_posts() ) : $post_query_first->the_post();
						$post_id = get_the_ID();
						$terms = get_the_terms( $post_id, 'category' );
					?> 
                    <?php
						$widget_1_big = wp_get_attachment_image_src(get_post_thumbnail_id(), 'pluto-737-983');
                        if ($widget_1_big) { ?>
                <div class="pluto-theme-logo-image w-60 w-sm-100">
                    <a href="#">  <img src="<?php echo esc_url($widget_1_big[0]); ?>" alt="<?php the_title_attribute(); ?>" class="space-desktop-view"></a>
                </div>
                <div class="pluto-theme-title w-45 text-center d-flex flex-column justify-content-between w-sm-100">
                    <span class="post-category f-s-sm-4"><?php foreach( $terms as $term ){ ?><span><?php echo esc_html($term->name); ?></span> <?php } ?></span>
                    <div class="pluto-theme-post">
                        <a class="h3 f-s-7 f-s-sm-5 f-w-bold" href="#"><?php get_the_title() ? the_title() : the_ID(); ?></a>
                    </div>
                    <span class="read-review f-s-sm-4">20 Min Read</span>
                </div>
                <?php } ?>
                <?php
						endwhile;
						wp_reset_postdata();
					?>  
            </div>
            <div class="pluto-theme-double-column d-flex flex-sm-column">
                <div class="pluto-theme-column w-50 d-flex align-items-center w-sm-100 flex-column">
                <?php 
							$post_query_second = new WP_Query( $second_args );
							while ( $post_query_second->have_posts() ) : $post_query_second->the_post();
                            $post_id = get_the_ID();
                            $terms = get_the_terms( $post_id, 'category' );
						?>
                         <?php $widget_2_small = wp_get_attachment_image_src(get_post_thumbnail_id(), 'pluto-450-450'); if ($widget_2_small) { ?>
                    <div class="pluto-theme-image w-60 w-sm-100">
                        <a href="#"> <img src="<?php echo esc_url($widget_2_small[0]); ?>" alt="<?php the_title_attribute(); ?>"></a>
                    </div>
                    <div class="pluto-theme-title w-50 text-center d-flex flex-column justify-content-between w-sm-100">
                        <span class="post-category f-s-4"><?php foreach( $terms as $term ){ ?><span><?php echo esc_html($term->name); ?></span> <?php } ?></span>
                        <div class="pluto-theme-post">
                            <a class="h3 f-s-5 f-w-bold " href="#"><?php get_the_title() ? the_title() : the_ID(); ?></a>
                        </div>
                        <span class="read-review f-s-4">20 Min Read</span>
                        <?php } ?>
                <?php endwhile; ?>
                    </div>
                </div>
                <div class="pluto-theme-column  w-50 d-flex align-items-center w-sm-100 flex-column">
                <?php while ( $post_query_first->have_posts() ) : $post_query_first->the_post();
						$post_id = get_the_ID();
						$terms = get_the_terms( $post_id, 'category' );
					?>
                    <?php
						$widget_1_big = wp_get_attachment_image_src(get_post_thumbnail_id(), 'pluto-737-983');
                        if ($widget_1_big) { ?>
                    <div class="pluto-theme-image w-60 w-sm-100">
                        <a href="#">  <img src="<?php echo esc_url($widget_1_big[0]); ?>" alt="<?php the_title_attribute(); ?>" class="space-desktop-view"></a>
                    </div>
                    <div class="pluto-theme-title w-50 text-center d-flex flex-column justify-content-between w-sm-100">
                        <span class="post-category f-s-4"><?php foreach( $terms as $term ){ ?><span><?php echo esc_html($term->name); ?></span> <?php } ?></span>
                        <div class="pluto-theme-post">
                            <a class="h3 f-s-5 f-w-bold" href="#"><?php get_the_title() ? the_title() : the_ID(); ?></a>
                        </div>
                        <span class="read-review f-s-4">20 Min Read</span>
                    </div>
                </div>
                <?php } ?>
                <?php
						endwhile;
						wp_reset_postdata();
					?> 
            </div>
        </div>
    </section>
</div>