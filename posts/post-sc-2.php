<section class="pluto-theme-post-layout-two"> 
    <div class="post-layout-two-list-box">
        <div class="pluto-theme-top-row d-flex flex-wrap">
            <div class="pluto-theme-column-one w-50 w-lg-100">
            <?php while ( $post_query_first->have_posts() ) : $post_query_first->the_post();
						$post_id = get_the_ID();
						$terms = get_the_terms( $post_id, 'category' );
					?>
                    <?php
						$widget_1_big = wp_get_attachment_image_src(get_post_thumbnail_id(), 'pluto-737-983');
						// $widget_3_big_mobile = wp_get_attachment_image_src(get_post_thumbnail_id(), 'pluto-450-600');
    
					if ($widget_1_big) { ?>
                <div class="pluto-theme-image ">
                <img src="<?php echo esc_url($widget_1_big[0]); ?>" alt="<?php the_title_attribute(); ?>" class="space-desktop-view">
                </div>
                <div class="post-content">
                    <div class="post-category f-s-3 f-w-regular">
                        <a class="category" href="#">
                        <?php foreach( $terms as $term ){ ?><span><?php echo esc_html($term->name); ?></span> <?php } ?>
                        </a>
                    </div>
                    <div class="post-title">
                        <a class="title-link f-s-9 f-w-medium" href="#"> <?php get_the_title() ? the_title() : the_ID(); ?></a>
                    </div>
                    <div class="events d-flex align-items-center">
                        <ul class="d-flex flex-wrap align-items-center">
                            <li class="d-flex align-items-center f-w-regular f-s-3"> <?php echo get_the_author(); ?></li>
                            <li class="d-flex align-items-center f-w-regular f-s-3">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/calendar-grey.svg" alt="pluto-theme-image">
                            <?php echo get_the_date(); ?>
                            </li>
                            <li class="d-flex align-items-center f-w-regular f-s-3"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/visibility-grey.svg" alt="">
                            <!-- <?php echo esc_html(spacethemes_get_post_views(get_the_ID())); ?>  -->
                           2 Min Read</li>
                        </ul>
                    </div>
                    <div class="description f-s-4 f-w-regular">
                        <p><?php echo get_the_content(); ?></p>
                    </div>
                </div>
                <?php } ?>
					<?php
						endwhile;
						wp_reset_postdata();
					?>
            </div>

            <div class="pluto-theme-column-two w-50 w-lg-100">
                <div class="row-one d-flex flex-wrap justify-content-between w-lg-100">
                <?php while ( $post_query_first->have_posts() ) : $post_query_first->the_post();
						$post_id = get_the_ID();
						$terms = get_the_terms( $post_id, 'category' );
					?>
                   <?php $widget_1_small = wp_get_attachment_image_src(get_post_thumbnail_id(), 'pluto-450-450'); 
                   if ($widget_1_small) { ?>
                    <div class="column w-50 w-sm-100">    
                        <div class="pluto-theme-image w-100">
                        <img src="<?php echo esc_url($widget_1_small[0]); ?>" alt="<?php the_title_attribute(); ?>">
                            <!-- <img class="w-100" src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/games/rise-of-kingdoms.png" alt="pluto-theme-image"> -->
                            <div class="space-overlay w-100"></div>
                        </div>
                        <div class="post-content">
                            <div class="post-category category-one">
                                <a class="category f-s-3 f-w-regular" href="#">
                                <?php foreach( $terms as $term ){ ?><span><?php echo esc_html($term->name); ?></span> <?php } ?>
                                </a>
                            </div>
                            <div class="post-title">
                                <a class="h3 f-s-5 f-w-medium" href="#"><?php get_the_title() ? the_title() : the_ID(); ?></a>
                            </div>
                            <div class="description d-flex">
                                <ul class="d-flex flex-wrap align-items-center">
                                    <li class="d-flex align-items-center f-s-3 f-w-regular"><?php echo get_the_author(); ?></li>
                                    <li class="d-flex align-items-center f-s-3 f-w-regular"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/calendar-grey.svg" alt="pluto-theme-image">
                                        <?php echo get_the_date(); ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                    <div class="column w-50 w-sm-100">
                        <div class="pluto-theme-image w-100">
                            <img class="w-100" src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/games/rise-of-kingdoms.png" alt="pluto-theme-image">
                            <div class="space-overlay w-100"></div>
                        </div>
                        <div class="post-content">
                            <div class="post-category category-two">
                                <a class="category f-s-3 f-w-regular" href="#">
                                <?php foreach( $terms as $term ){ ?><span><?php echo esc_html($term->name); ?></span> <?php } ?>
                                </a>
                            </div>
                            <div class="post-title">
                                <a class="h3 f-s-5 f-w-medium" href="#"><?php get_the_title() ? the_title() : the_ID(); ?></a>
                            </div>
                            <div class="description d-flex">
                                <ul class="d-flex flex-wrap align-items-center">
                                    <li class="d-flex align-items-center f-s-3 f-w-regular"><?php echo get_the_author(); ?></li>
                                    <li class="d-flex align-items-center f-s-3 f-w-regular"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/calendar-grey.svg" alt="pluto-theme-image">
                                        <?php echo get_the_date(); ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="column w-50 w-sm-100">
                        <div class="pluto-theme-image w-100">
                            <img class="w-100" src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/games/rise-of-kingdoms.png" alt="pluto-theme-image">
                            <div class="space-overlay w-100"></div>
                        </div>
                        <div class="post-content">
                            <div class="post-category category-three">
                                <a class="category f-s-3 f-w-regular" href="#">
                                <?php foreach( $terms as $term ){ ?><span><?php echo esc_html($term->name); ?></span> <?php } ?>
                                </a>
                            </div>
                            <div class="post-title">
                                <a class="h3 f-s-5 f-w-medium" href="#"><?php get_the_title() ? the_title() : the_ID(); ?></a>
                            </div>
                            <div class="description d-flex">
                                <ul class="d-flex flex-wrap align-items-center">
                                    <li class="d-flex align-items-center f-s-3 f-w-regular"><?php echo get_the_author(); ?></li>
                                    <li class="d-flex align-items-center f-s-3 f-w-regular"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/calendar-grey.svg" alt="pluto-theme-image">
                                        <?php echo get_the_date(); ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="column w-50 w-sm-100">
                        <div class="pluto-theme-image w-100">
                            <img class="w-100" src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/games/rise-of-kingdoms.png" alt="pluto-theme-image">
                            <div class="space-overlay w-100"></div>
                        </div>
                        <div class="post-content">
                            <div class="post-category category-four">
                                <a class="category f-s-3 f-w-regular" href="#">
                                <?php foreach( $terms as $term ){ ?><span><?php echo esc_html($term->name); ?></span> <?php } ?>
                                </a>
                            </div>
                            <div class="post-title f-s-5 f-w-medium">
                                <a class="h3 f-s-5 f-w-medium" href="#"><?php get_the_title() ? the_title() : the_ID(); ?></a>
                            </div>
                            <div class="description d-flex">
                                <ul class="d-flex flex-wrap align-items-center">
                                    <li class="d-flex align-items-center f-s-3 f-w-regular"><?php echo get_the_author(); ?></li>
                                    <li class="d-flex align-items-center f-s-3 f-w-regular"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/calendar-grey.svg" alt="pluto-theme-image">
                                        <?php echo get_the_date(); ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                     <?php endwhile; ?>
                </div>
            </div>
        </div>
    </div>
</section>