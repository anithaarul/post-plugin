<div class="pluto-theme-container pluto-theme-padding-tb-default">
        <section class="pluto-theme-post-layout-eight">
            <div class="pluto-theme-list-box w-100 d-flex flex-wrap justify-content-between">
            <?php while ( $post_query_first->have_posts() ) : $post_query_first->the_post();
						$post_id = get_the_ID();
						$terms = get_the_terms( $post_id, 'category' );
					?> 
                    <?php
						$widget_1_big = wp_get_attachment_image_src(get_post_thumbnail_id(), 'pluto-737-983');
                        if ($widget_1_big) { ?>
                <div class="column w-25 w-lg-50 w-xsm-100">
                    <div class="pluto-theme-column d-flex flex-column justify-content-between">
                        <div>
                            <div class="pluto-theme-top-row d-flex justify-content-center">
                                <div class="pluto-theme-image d-flex justify-content-center">
                                <img src="<?php echo esc_url($widget_1_big[0]); ?>" alt="<?php the_title_attribute(); ?>" class="space-desktop-view">
                                    <div class="pluto-theme-badge">
                                        <span class="post-category d-flex align-items-center f-s-5 f-w-medium">
                                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/icons/common/white-tag.png" alt="#"> 
                                        <?php the_category(' '); ?></span>
                                    </div>
                                </div>
                            </div>
                        <div class="post-content">
                            <div class="post-title">
                                <a class="h3 f-s-6 f-w-medium" href="#"><?php the_title_attribute(); ?></a>
                            </div>
                            <ul class="d-flex flex-column">
                            <li class="d-flex align-items-center f-s-5 f-w-regular"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/feather-white.svg" alt="pluto-theme-image"><?php echo get_the_author(); ?></li>
                            <li class="d-flex align-items-center f-s-5 f-w-regular"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/timelapse-white.svg" alt="pluto-theme-image"><?php printf( esc_html_x( '%s ago', '%s = human-readable time difference', 'spacethemes' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></li>
                            <li class="d-flex align-items-center f-s-5 f-w-regular"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/visibility-white.svg" alt="pluto-theme-image">1441 Views</li>
                            </ul>
                        </div>
                        
                    </div>
                        <div class="pluto-theme-button w-100 d-flex align-items-center justify-content-center">
                            <a href="#" class="pluto-theme-primary-affiliate-button f-s-5 f-w-bold"><span>Read More</span></a>
                        </div>
                    </div>
                </div>
                
                <div class="column w-25 w-lg-50 w-xsm-100">
                    <div class="pluto-theme-column d-flex flex-column justify-content-between">
                        <div>
                            <div class="pluto-theme-top-row d-flex justify-content-center">
                                <div class="pluto-theme-image d-flex justify-content-center">
                                <img src="<?php echo esc_url($widget_1_big[0]); ?>" alt="<?php the_title_attribute(); ?>" class="space-desktop-view">
                                    <div class="pluto-theme-badge">
                                        <span class="post-category d-flex align-items-center f-s-5 f-w-medium">
                                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/icons/common/white-tag.png" alt="#"> 
                                        <?php the_category(' '); ?></span>
                                    </div>
                                </div>
                            </div>
                        <div class="post-content">
                            <div class="post-title">
                                <a class="h3 f-s-6 f-w-medium" href="#"><?php the_title_attribute(); ?></a>
                            </div>
                            <ul class="d-flex flex-column">
                            <li class="d-flex align-items-center f-s-5 f-w-regular"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/feather-white.svg" alt="pluto-theme-image"><?php echo get_the_author(); ?></li>
                            <li class="d-flex align-items-center f-s-5 f-w-regular"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/timelapse-white.svg" alt="pluto-theme-image"><?php printf( esc_html_x( '%s ago', '%s = human-readable time difference', 'spacethemes' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></li>
                            <li class="d-flex align-items-center f-s-5 f-w-regular"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/visibility-white.svg" alt="pluto-theme-image">1441 Views</li>
                            </ul>
                        </div>
                        
                    </div>
                        <div class="pluto-theme-button w-100 d-flex align-items-center justify-content-center">
                            <a href="#" class="pluto-theme-primary-affiliate-button f-s-5 f-w-bold"><span>Read More</span></a>
                        </div>
                    </div>
                </div>

                <div class="column w-25 w-lg-50 w-xsm-100">
                    <div class="pluto-theme-column d-flex flex-column justify-content-between">
                        <div>
                            <div class="pluto-theme-top-row d-flex justify-content-center">
                                <div class="pluto-theme-image d-flex justify-content-center">
                                <img src="<?php echo esc_url($widget_1_big[0]); ?>" alt="<?php the_title_attribute(); ?>" class="space-desktop-view">
                                    <div class="pluto-theme-badge">
                                        <span class="post-category d-flex align-items-center f-s-5 f-w-medium">
                                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/icons/common/white-tag.png" alt="#"> 
                                        <?php the_category(' '); ?></span>
                                    </div>
                                </div>
                            </div>
                        <div class="post-content">
                            <div class="post-title">
                                <a class="h3 f-s-6 f-w-medium" href="#"><?php the_title_attribute(); ?></a>
                            </div>
                            <ul class="d-flex flex-column">
                            <li class="d-flex align-items-center f-s-5 f-w-regular"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/feather-white.svg" alt="pluto-theme-image"><?php echo get_the_author(); ?></li>
                            <li class="d-flex align-items-center f-s-5 f-w-regular"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/timelapse-white.svg" alt="pluto-theme-image"><?php printf( esc_html_x( '%s ago', '%s = human-readable time difference', 'spacethemes' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></li>
                            <li class="d-flex align-items-center f-s-5 f-w-regular"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/visibility-white.svg" alt="pluto-theme-image">1441 Views</li>
                            </ul>
                        </div>
                        
                    </div>
                        <div class="pluto-theme-button w-100 d-flex align-items-center justify-content-center">
                            <a href="#" class="pluto-theme-primary-affiliate-button f-s-5 f-w-bold"><span>Read More</span></a>
                        </div>
                    </div>
                </div>

                <div class="column w-25 w-lg-50 w-xsm-100">
                    <div class="pluto-theme-column d-flex flex-column justify-content-between">
                        <div>
                            <div class="pluto-theme-top-row d-flex justify-content-center">
                                <div class="pluto-theme-image d-flex justify-content-center">
                                <img src="<?php echo esc_url($widget_1_big[0]); ?>" alt="<?php the_title_attribute(); ?>" class="space-desktop-view">
                                    <div class="pluto-theme-badge">
                                        <span class="post-category d-flex align-items-center f-s-5 f-w-medium">
                                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/icons/common/white-tag.png" alt="#"> 
                                        <?php the_category(' '); ?></span>
                                    </div>
                                </div>
                            </div>
                        <div class="post-content">
                            <div class="post-title">
                                <a class="h3 f-s-6 f-w-medium" href="#"><?php the_title_attribute(); ?></a>
                            </div>
                            <ul class="d-flex flex-column">
                            <li class="d-flex align-items-center f-s-5 f-w-regular"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/feather-white.svg" alt="pluto-theme-image"><?php echo get_the_author(); ?></li>
                            <li class="d-flex align-items-center f-s-5 f-w-regular"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/timelapse-white.svg" alt="pluto-theme-image"><?php printf( esc_html_x( '%s ago', '%s = human-readable time difference', 'spacethemes' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></li>
                            <li class="d-flex align-items-center f-s-5 f-w-regular"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/visibility-white.svg" alt="pluto-theme-image">1441 Views</li>
                            </ul>
                        </div>
                        
                    </div>
                        <div class="pluto-theme-button w-100 d-flex align-items-center justify-content-center">
                            <a href="#" class="pluto-theme-primary-affiliate-button f-s-5 f-w-bold"><span>Read More</span></a>
                        </div>
                    </div>
                </div>
                </div>
                <?php } ?>
                <?php
						endwhile;
						wp_reset_postdata();
					?> 
                
                
            </div>
        </section>
    </div>