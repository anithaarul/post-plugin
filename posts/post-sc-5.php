<section class="pluto-theme-post-layout-five">
    <div class="post-layout-five-list-box"> 
        <div class="pluto-theme-top-row d-flex flex-md-column">
        <?php while ( $post_query_first->have_posts() ) : $post_query_first->the_post();
						$post_id = get_the_ID();
						$terms = get_the_terms( $post_id, 'category' );
					?>
                    <?php
						$widget_1_big = wp_get_attachment_image_src(get_post_thumbnail_id(), 'pluto-737-983');
                        if ($widget_1_big) { ?>
            <div class="column-one w-40 w-md-100">
           
                <div class="pluto-theme-image w-100">
                    <a href="#">
                    <img src="<?php echo esc_url($widget_1_big[0]); ?>" alt="<?php the_title_attribute(); ?>" class="space-desktop-view">
                    </a>
                </div>  
                      
            </div>            
            <div class="column-two w-60 w-md-100">
                <div class="post-content f-s-5 f-w-normal">
                    <div class="pluto-theme-title">
                        <a class="h3 f-s-12 f-s-xxsm-9" href="#"><?php the_title_attribute(); ?></a>
                    </div>
                    <span class="post-category"><?php the_category(' '); ?></span>
                    <ul class="d-flex flex-wrap align-items-center">
                        <li class="f-s-6 f-w-normal">
                        <?php printf( esc_html_x( '%s ago', '%s = human-readable time difference', 'spacethemes' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?>
                        </li>
                        <li class="f-s-6 f-w-normal">  <?php echo get_the_author(); ?></li>
                    </ul>
                    <p class="short-description f-w-normal f-s-5"><?php echo get_the_content(); ?></p>
                </div>
            </div>
            <?php } ?>
                <?php
						endwhile;
						wp_reset_postdata();
					?>  
        </div>
    </div>
</section>