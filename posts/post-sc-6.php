<section class="pluto-theme-post-layout-six">
    <div class="pluto-theme-container pluto-theme-padding-tb-default">
        <div class="post-layout-six-list-box"> 
            <div class="pluto-theme-top-row d-flex flex-md-column">
            <?php while ( $post_query_first->have_posts() ) : $post_query_first->the_post();
						$post_id = get_the_ID();
						$terms = get_the_terms( $post_id, 'category' );
					?>
                    <?php
						$widget_1_big = wp_get_attachment_image_src(get_post_thumbnail_id(), 'pluto-737-983');
                        if ($widget_1_big) { ?>
                <div class="column-one w-25 w-md-100">
                    <div class="pluto-theme-image w-100">
                    <img src="<?php echo esc_url($widget_1_big[0]); ?>" alt="<?php the_title_attribute(); ?>" class="space-desktop-view">
                    </div>
                </div>
                <div class="column-two w-50 d-flex flex-column justiy-content-between w-md-100">
                    <h3 class="h3 f-s-9 f-w-medium"><?php the_title_attribute(); ?></h3>
                    <ul>
                        <li class="d-flex align-items-center f-s-6 f-w-normal"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/feather.svg" alt="pluto-theme-image"> <?php echo get_the_author(); ?>
                        </li>
                        <li class="d-flex align-items-center f-s-6 f-w-normal"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/timelapse.svg" alt="pluto-theme-image"> <?php printf( esc_html_x( '%s ago', '%s = human-readable time difference', 'spacethemes' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?>
                        </li>
                    </ul>
                </div>
                <div class="column-three w-25 d-flex align-items-end w-md-100">
                    <div class="button w-100 d-flex justify-content-end">
                        <a href="#" class="pluto-theme-primary-affiliate-button w-90 f-s-8 f-w-normal d-flex justify-content-evenly justify-content-md-center w-md-100">Read Now<img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/arrow-right-white.svg" alt="pluto-theme-image"></a>
                    </div>
                </div>
                <?php } ?>
                <?php
						endwhile;
						wp_reset_postdata();
					?>  
            </div>

        </div>
    </div>
</section>