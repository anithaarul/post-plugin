<section class="pluto-theme-post-layout-three">
    <div class="post-layout-three-list-box"> 
        <div class="pluto-theme-top-row">
            <article class="pluto-theme-row-one d-flex flex-md-column w-sm-100 flex-sm-column">
            <?php while ( $post_query_first->have_posts() ) : $post_query_first->the_post();
						$post_id = get_the_ID();
						$terms = get_the_terms( $post_id, 'category' );
					?>
                    <?php
						$widget_1_small = wp_get_attachment_image_src(get_post_thumbnail_id(), 'pluto-737-983');
						// $widget_3_big_mobile = wp_get_attachment_image_src(get_post_thumbnail_id(), 'pluto-450-600');
    
					if ($widget_1_small) { ?>
                <div class="pluto-theme-image w-65 w-md-100 w-sm-100">
                    <a href="#">
                    <img src="<?php echo esc_url($widget_1_small[0]); ?>" alt="<?php the_title_attribute(); ?>" class="space-desktop-view">
                    </a>
                </div>
                <div class="post-content w-35 w-md-100">
                    <div class="events d-flex align-items-center">
                        <ul class="d-flex flex-wrap">
                            <li class="f-s-3 f-w-medium">
                            <?php echo esc_html($terms[0]->name); ?>
                            </li>
                            <li class="f-s-3 f-w-medium"> <?php echo esc_html($terms[1]->name); ?></li>
                            <li class="f-s-3 f-w-medium"><?php echo get_the_date(); ?></li>
                        </ul>
                    </div>
                    <a class="h3 f-s-10 f-w-medium" href="#"> <?php get_the_title() ? the_title() : the_ID(); ?></a>
                    <div class="description">
                        <p class="f-s-5 f-w-regular"><?php echo get_the_content(); ?></p>
                    </div>
                    <div class="profile d-flex align-items-center">
                        <div class="profile-image">
                        <?php
                           global $current_user;    
                           echo get_avatar( $current_user->ID, 64 );  ?>
                        </div>
                        <div class="profile-description d-flex flex-column">
                            <p class="f-s-4 f-w-medium">  <?php echo get_the_author(); ?></p>
                            <span class="f-s-3 f-w-regular">Marketing Head</span>
                        </div>
                    </div>
                </div>
                <?php } ?>
					<?php
						endwhile;
						wp_reset_postdata();
					?>
            </article>
            <div class="pluto-theme-row-two d-flex flex-wrap justify-content-between w-sm-100">
            <?php 
							$post_query_second = new WP_Query( $second_args );
							while ( $post_query_second->have_posts() ) : $post_query_second->the_post();
                            $post_id = get_the_ID();
                            $terms = get_the_terms( $post_id, 'category' );
						?>
                        <?php $widget_2_small = wp_get_attachment_image_src(get_post_thumbnail_id(), 'pluto-450-450'); if ($widget_2_small) { ?>
                <div class="column w-33 w-sm-100">
                    <div class="pluto-theme-image w-100 w-sm-100">
                        <a href="#">
                        <img src="<?php echo esc_url($widget_2_small[0]); ?>" alt="<?php the_title_attribute(); ?>">
                        <!-- <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/posts/Fantasy-Westward.png" alt="pluto-theme-image"> -->
                    </a>
                    </div>
                    <div class="post-content">
                        <div class="events d-flex align-items-center">
                            <ul class="d-flex">
                                <li class="f-s-3 f-w-medium">
                                <?php foreach( $terms as $term ){ ?><span><?php echo esc_html($term->name); ?></span> <?php } ?>
                                </li>
                                <li class="f-s-3 f-w-medium">
                                <?php echo get_the_date(); ?>
                                </li>
                            </ul>
                        </div>
                        <a class="h3 f-s-7 f-w-medium" href="#">
                        <?php get_the_title() ? the_title() : the_ID(); ?>
                        </a>
                    </div>
                </div>
                <div class="column w-33 w-sm-100">
                    <div class="pluto-theme-image w-100 w-sm-100">
                        <a href="#">
                        <img src="<?php echo esc_url($widget_2_small[0]); ?>" alt="<?php the_title_attribute(); ?>">
                        <!-- <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/posts/Fantasy-Westward.png" alt="pluto-theme-image"> -->
                    </a>
                    </div>
                    <div class="post-content">
                        <div class="events d-flex align-items-center">
                            <ul class="d-flex">
                                <li class="f-s-3 f-w-medium">
                                <?php foreach( $terms as $term ){ ?><span><?php echo esc_html($term->name); ?></span> <?php } ?>
                                </li>
                                <li class="f-s-3 f-w-medium">
                                <?php echo get_the_date(); ?>
                                </li>
                            </ul>
                        </div>
                        <a class="h3 f-s-7 f-w-medium" href="#">
                        <?php get_the_title() ? the_title() : the_ID(); ?>
                        </a>
                    </div>
                </div>
                <div class="column w-33 w-sm-100">
                    <div class="pluto-theme-image w-100 w-sm-100">
                        <a href="#">
                        <img src="<?php echo esc_url($widget_2_small[0]); ?>" alt="<?php the_title_attribute(); ?>">
                        <!-- <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/posts/Fantasy-Westward.png" alt="pluto-theme-image"> -->
                    </a>
                    </div>
                    <div class="post-content">
                        <div class="events d-flex align-items-center">
                            <ul class="d-flex">
                                <li class="f-s-3 f-w-medium">
                                <?php foreach( $terms as $term ){ ?><span><?php echo esc_html($term->name); ?></span> <?php } ?>
                                </li>
                                <li class="f-s-3 f-w-medium">
                                <?php echo get_the_date(); ?>
                                </li>
                            </ul>
                        </div>
                        <a class="h3 f-s-7 f-w-medium" href="#">
                        <?php get_the_title() ? the_title() : the_ID(); ?>
                        </a>
                    </div>
                </div>
                <div class="column w-33 w-sm-100">
                    <div class="pluto-theme-image w-100 w-sm-100">
                        <a href="#">
                        <img src="<?php echo esc_url($widget_2_small[0]); ?>" alt="<?php the_title_attribute(); ?>">
                        <!-- <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/posts/Fantasy-Westward.png" alt="pluto-theme-image"> -->
                    </a>
                    </div>
                    <div class="post-content">
                        <div class="events d-flex align-items-center">
                            <ul class="d-flex">
                                <li class="f-s-3 f-w-medium">
                                <?php foreach( $terms as $term ){ ?><span><?php echo esc_html($term->name); ?></span> <?php } ?>
                                </li>
                                <li class="f-s-3 f-w-medium">
                                <?php echo get_the_date(); ?>
                                </li>
                            </ul>
                        </div>
                        <a class="h3 f-s-7 f-w-medium" href="#">
                        <?php get_the_title() ? the_title() : the_ID(); ?>
                        </a>
                    </div>
                </div>
                <div class="column w-33 w-sm-100">
                    <div class="pluto-theme-image w-100 w-sm-100">
                        <a href="#">
                        <img src="<?php echo esc_url($widget_2_small[0]); ?>" alt="<?php the_title_attribute(); ?>">
                        <!-- <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/posts/Fantasy-Westward.png" alt="pluto-theme-image"> -->
                    </a>
                    </div>
                    <div class="post-content">
                        <div class="events d-flex align-items-center">
                            <ul class="d-flex">
                                <li class="f-s-3 f-w-medium">
                                <?php foreach( $terms as $term ){ ?><span><?php echo esc_html($term->name); ?></span> <?php } ?>
                                </li>
                                <li class="f-s-3 f-w-medium">
                                <?php echo get_the_date(); ?>
                                </li>
                            </ul>
                        </div>
                        <a class="h3 f-s-7 f-w-medium" href="#">
                        <?php get_the_title() ? the_title() : the_ID(); ?>
                        </a>
                    </div>
                </div>
                <div class="column w-33 w-sm-100">
                    <div class="pluto-theme-image w-100 w-sm-100">
                        <a href="#">
                        <img src="<?php echo esc_url($widget_2_small[0]); ?>" alt="<?php the_title_attribute(); ?>">
                        <!-- <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/posts/Fantasy-Westward.png" alt="pluto-theme-image"> -->
                    </a>
                    </div>
                    <div class="post-content">
                        <div class="events d-flex align-items-center">
                            <ul class="d-flex">
                                <li class="f-s-3 f-w-medium">
                                <?php foreach( $terms as $term ){ ?><span><?php echo esc_html($term->name); ?></span> <?php } ?>
                                </li>
                                <li class="f-s-3 f-w-medium">
                                <?php echo get_the_date(); ?>
                                </li>
                            </ul>
                        </div>
                        <a class="h3 f-s-7 f-w-medium" href="#">
                        <?php get_the_title() ? the_title() : the_ID(); ?>
                        </a>
                    </div>
                </div>
                <?php } ?>
                            <?php endwhile; ?>
            </div>
        </div>
    </div>
</section>