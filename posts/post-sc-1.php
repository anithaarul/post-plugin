<section class="pluto-theme-post-layout-one"> 
        <div class="pluto-theme-container pluto-theme-padding-tb-default">
            <div class="post-layout-one-list-box">
                <div class="pluto-theme-top-row d-flex flex-sm-wrap">
                    <div class="pluto-theme-column-one w-50 w-sm-100">
                    <?php while ( $post_query_first->have_posts() ) : $post_query_first->the_post();
						$post_id = get_the_ID();
						$terms = get_the_terms( $post_id, 'category' );
					?>
                    <?php
						$widget_1_big = wp_get_attachment_image_src(get_post_thumbnail_id(), 'pluto-737-983');
						// $widget_3_big_mobile = wp_get_attachment_image_src(get_post_thumbnail_id(), 'pluto-450-600');
    
					if ($widget_1_big) { ?>
                        <div class="pluto-theme-image">
                        <img src="<?php echo esc_url($widget_1_big[0]); ?>" alt="<?php the_title_attribute(); ?>" class="space-desktop-view">
                            <!-- <img src="../../assets/images/posts/rise-of-kingdoms.png" alt="pluto-theme-image"> -->
                        </div>
                        <div class="post-content">
                            <span class="post-category f-s-3 f-w-regular">
                            <?php foreach( $terms as $term ){ ?><span><?php echo esc_html($term->name); ?></span> <?php } ?>
                            <!-- Games -->
                         </span>
                            <div class="post-title">
                                <a class="h3 f-s-9 f-w-medium" href="#">
                                <?php get_the_title() ? the_title() : the_ID(); ?>
                                <!-- Best Online Games to play during Christmas Season and Win Real Cash -->
                            </a>
                            </div>
                            <ul class="d-flex">
                                <li class="f-s-3 f-w-medium">
                                <?php echo get_the_author(); ?>
                                <!-- Van Dees</li> -->
                                <li class="f-s-3 f-w-medium">
                                <?php echo get_the_date(); ?>
                                    <!-- 04 November 2021 -->
                                </li> 
                            </ul>
                            <?php if ( has_post_format( 'video' )) { ?>
									<div class="space-post-format absolute"><i class="fas fa-play"></i></div>
								<?php } ?>
								<?php if ( has_post_format( 'image' )) { ?>
									<div class="space-post-format absolute"><i class="fas fa-camera"></i></div>
								<?php } ?>
								<?php if ( has_post_format( 'gallery' )) { ?>
									<div class="space-post-format absolute"><i class="fas fa-camera"></i></div>
								<?php } ?>
                        </div>
                        <?php } ?>
					<?php
						endwhile;
						wp_reset_postdata();
					?>
                    </div>
                    <div class="pluto-theme-column-two w-50 d-flex flex-wrap w-sm-100">
                        <div class="pluto-theme-row-one w-100">
                        <?php 
							$post_query_second = new WP_Query( $second_args );
							while ( $post_query_second->have_posts() ) : $post_query_second->the_post();
                            $post_id = get_the_ID();
                            $terms = get_the_terms( $post_id, 'category' );
						?>
                        <?php $widget_2_small = wp_get_attachment_image_src(get_post_thumbnail_id(), 'pluto-450-450'); if ($widget_2_small) { ?>
                            <div class="pluto-theme-image">
                            <img src="<?php echo esc_url($widget_2_small[0]); ?>" alt="<?php the_title_attribute(); ?>">
                                <!-- <img src="../../assets/images/posts/gambling.png" alt="pluto-theme-image"> -->
                                <?php if ( has_post_format( 'video' )) { ?>
												<div class="space-post-format absolute"><i class="fas fa-play"></i></div>
											<?php } ?>
											<?php if ( has_post_format( 'image' )) { ?>
												<div class="space-post-format absolute"><i class="fas fa-camera"></i></div>
											<?php } ?>
											<?php if ( has_post_format( 'gallery' )) { ?>
												<div class="space-post-format absolute"><i class="fas fa-camera"></i></div>
											<?php } ?>
                            </div>
                            <div class="post-content">
                                <span class="post-category category-one f-s-3 f-w-regular "><?php the_category(' '); ?></span>
                                <div class="post-title">
                                    <a class="h3 f-s-9 f-w-medium"  href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php get_the_title() ? the_title() : the_ID(); ?></a>
                                </div>
                                <ul class="d-flex">
                                    <li class="f-s-3 f-w-medium">
                                    <?php echo get_the_author(); ?>
                                <!-- Van Dees</li> -->
                                    </li>
                                    <li class="f-s-3 f-w-medium">
                                    <?php echo get_the_date(); ?>
                                    <!-- 04 November 2021 -->
                                    </li>
                                </ul>
                            </div>
                            <?php } ?>
                            <?php endwhile; ?>
                        </div>
                        <div class="pluto-theme-row-one w-100">
                                <?php
                            $post_query_third = new WP_Query( $third_args );
                            while ( $post_query_third->have_posts() ) : $post_query_third->the_post();
                            $post_id = get_the_ID();
                            $terms = get_the_terms( $post_id, 'category' );
                        ?>
                        <?php
                            $widget_3_small = wp_get_attachment_image_src(get_post_thumbnail_id(), 'pluto-270-430');
                            if ($widget_3_small) {
                        ?>
                            <div class="pluto-theme-image">
                            <img src="<?php echo esc_url($widget_3_small[0]); ?>" alt="<?php the_title_attribute(); ?>" class="space-desktop-view-2">
                                <!-- <img src="../../assets/images/posts/gambling.png" alt="pluto-theme-image"> -->
                                <?php if ( has_post_format( 'video' )) { ?>
												<div class="space-post-format absolute"><i class="fas fa-play"></i></div>
											<?php } ?>
											<?php if ( has_post_format( 'image' )) { ?>
												<div class="space-post-format absolute"><i class="fas fa-camera"></i></div>
											<?php } ?>
											<?php if ( has_post_format( 'gallery' )) { ?>
												<div class="space-post-format absolute"><i class="fas fa-camera"></i></div>
											<?php } ?>
                            </div>
                            <div class="post-content">
                                <span class="post-category category-one f-s-3 f-w-regular"><?php the_category(' '); ?></span>
                                <div class="post-title">
                                    <a class="h3 f-s-9 f-w-medium"  href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php get_the_title() ? the_title() : the_ID(); ?></a>
                                </div>
                                <ul class="d-flex">
                                    <li class="f-s-3 f-w-medium">
                                    <?php echo get_the_author(); ?>
                                <!-- Van Dees</li> -->
                                    </li>
                                    <li class="f-s-3 f-w-medium">
                                    <?php echo get_the_date(); ?>
                                    <!-- 04 November 2021 -->
                                </li>
                                </ul>
                            </div>
                            <?php } ?>
                            <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>