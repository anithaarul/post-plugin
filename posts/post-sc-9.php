<section class="pluto-theme-post-layout-nine">
        <div class="pluto-theme-container pluto-theme-padding-tb-default">
            <div class="post-layout-nine-list-box">
            <?php while ( $post_query_first->have_posts() ) : $post_query_first->the_post();
						$post_id = get_the_ID();
						$terms = get_the_terms( $post_id, 'category' ); 
					?>
                    <?php
						$widget_1_big = wp_get_attachment_image_src(get_post_thumbnail_id(), 'pluto-737-983');
                        if ($widget_1_big) { ?>
                <div class="pluto-theme-row d-flex flex-xsm-column">
                    <div class="column-one">
                        <div class="pluto-theme-image w-100">
                            <a href="#">
                            <img src="<?php echo esc_url($widget_1_big[0]); ?>" alt="<?php the_title_attribute(); ?>" class="space-desktop-view">
                                </a>
                        </div>
                    </div>
                    <div class="column-two">
                        <div class="post-content ">
                            <span class="post-category badge rounded-pill f-s-4 f-w-regular"><?php the_category(' '); ?></span>
                            <div class="post-title">
                                <a class="h3 f-w-bold f-s-11" href="#"><?php the_title_attribute(); ?></a>
                            </div>
                            <div class="short-description f-w-regular f-s-5"><?php echo get_the_content(); ?></div>
                            <ul class="d-flex align-items-center f-s-4 f-w-regular flex-sm-column align-items-sm-start">
                            <li class="d-flex align-items-center f-s-4 f-w-regular"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/timer-grey.svg" alt="pluto-theme-image"><?php printf( esc_html_x( '%s ago', '%s = human-readable time difference', 'spacethemes' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></li>
                            <li class="d-flex align-items-center f-s-4 f-w-regular"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/visibility-grey.svg" alt="pluto-theme-image">541 Views
                            </li>
                            <li class="d-flex align-items-center f-s-4 f-w-regular"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/message-grey.svg" alt="pluto-theme-image"><?php comments_number( '0', '1', '%' ); ?>
                            </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="pluto-theme-row d-flex flex-xsm-column">
                    <div class="column-one">
                        <div class="pluto-theme-image w-100">
                            <a href="#">
                            <img src="<?php echo esc_url($widget_1_big[0]); ?>" alt="<?php the_title_attribute(); ?>" class="space-desktop-view">
                                </a>
                        </div>
                    </div>
                    <div class="column-two">
                        <div class="post-content ">
                            <span class="post-category badge rounded-pill f-s-4 f-w-regular"><?php the_category(' '); ?></span>
                            <div class="post-title">
                                <a class="h3 f-w-bold f-s-11" href="#"><?php the_title_attribute(); ?></a>
                            </div>
                            <div class="short-description f-w-regular f-s-5"><?php echo get_the_content(); ?></div>
                            <ul class="d-flex align-items-center f-s-4 f-w-regular flex-sm-column align-items-sm-start">
                            <li class="d-flex align-items-center f-s-4 f-w-regular"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/timer-grey.svg" alt="pluto-theme-image"><?php printf( esc_html_x( '%s ago', '%s = human-readable time difference', 'spacethemes' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></li>
                            <li class="d-flex align-items-center f-s-4 f-w-regular"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/visibility-grey.svg" alt="pluto-theme-image">541 Views
                            </li>
                            <li class="d-flex align-items-center f-s-4 f-w-regular"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/message-grey.svg" alt="pluto-theme-image"><?php comments_number( '0', '1', '%' ); ?>
                            </li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <div class="pluto-theme-row d-flex flex-xsm-column">
                    <div class="column-one">
                        <div class="pluto-theme-image w-100">
                            <a href="#">
                            <img src="<?php echo esc_url($widget_1_big[0]); ?>" alt="<?php the_title_attribute(); ?>" class="space-desktop-view">
                                </a>
                        </div>
                    </div>
                    <div class="column-two">
                        <div class="post-content ">
                            <span class="post-category badge rounded-pill f-s-4 f-w-regular"><?php the_category(' '); ?></span>
                            <div class="post-title">
                                <a class="h3 f-w-bold f-s-11" href="#"><?php the_title_attribute(); ?></a>
                            </div>
                            <div class="short-description f-w-regular f-s-5"><?php echo get_the_content(); ?></div>
                            <ul class="d-flex align-items-center f-s-4 f-w-regular flex-sm-column align-items-sm-start">
                            <li class="d-flex align-items-center f-s-4 f-w-regular"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/timer-grey.svg" alt="pluto-theme-image"><?php printf( esc_html_x( '%s ago', '%s = human-readable time difference', 'spacethemes' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></li>
                            <li class="d-flex align-items-center f-s-4 f-w-regular"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/visibility-grey.svg" alt="pluto-theme-image">541 Views
                            </li>
                            <li class="d-flex align-items-center f-s-4 f-w-regular"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icons/common/message-grey.svg" alt="pluto-theme-image"><?php comments_number( '0', '1', '%' ); ?>
                            </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <?php } ?>
                <?php
						endwhile;
						wp_reset_postdata();
					?> 
            </div>
        </div>
    </section>