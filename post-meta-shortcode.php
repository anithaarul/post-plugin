<?php
 
/*
 
Plugin Name: Custom Meta Box & Shortcode
 
Plugin URI:  www.example.com
 
Description: Creates a meta box in WordPress
 
Version:     1.0.0 
 
Author:      Dhamayanthi
 
Author URI:  www.example.com
 
License: GPL2
 
*/

/*  Post template style Start */

function pluto_register_post_style() {
    add_meta_box( 'meta-box-id', esc_html__( 'Post template style', 'pluto' ), 'pluto_post_style_callback', 'post', 'side', 'high' );
}
add_action( 'add_meta_boxes', 'pluto_register_post_style' );

function pluto_post_style_callback( $post ) {
	wp_nonce_field( 'post_style_box', 'post_style_nonce' );
    $post_style = get_post_meta( $post->ID, 'post_style', true );
?>
    <?php $selected = ' selected'; ?>
    <select id="post_style" name="post_style" style="width: calc(100% - 32px);">
     <option value="1"<?php if ( $post_style == '1') echo esc_html( $selected ); ?>><?php esc_html_e( 'Style 1', 'pluto' ); ?></option>
     <option value="2"<?php if ( $post_style == '2') echo esc_html( $selected ); ?>><?php esc_html_e( 'Style 2', 'pluto' ); ?></option>
    </select>
<?php
}

function pluto_post_style_save( $post_id ) {

        if ( ! isset( $_POST['post_style_nonce'] ) ) {
            return $post_id;
        }
 
        $nonce = $_POST['post_style_nonce'];

        if ( ! wp_verify_nonce( $nonce, 'post_style_box' ) ) {
            return $post_id;
        }

        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return $post_id;
        }

        if ( 'page' == $_POST['post_type'] ) {
            if ( ! current_user_can( 'edit_page', $post_id ) ) {
                return $post_id;
            }
        } else {
            if ( ! current_user_can( 'edit_post', $post_id ) ) {
                return $post_id;
            }
        }

        $post_style_data = sanitize_text_field( $_POST['post_style'] );

        update_post_meta( $post_id, 'post_style', $post_style_data );
}
add_action( 'save_post', 'pluto_post_style_save' );

/*  Post template style End */

/** PROS AND CONS START */

add_action( 'admin_init', 'pluto_posts_pros_cons_fields' );

function pluto_posts_pros_cons_fields() {
    add_meta_box( 'pluto_posts_pros_cons_meta_box',
        esc_html__( 'Pros/Cons of the post', 'pluto' ),
        'pluto_posts_pros_cons_display_meta_box',
        'post', 'normal', 'high'
    );
}

function pluto_posts_pros_cons_display_meta_box( $post ) {

	wp_nonce_field( 'pluto_posts_pros_cons_box', 'pluto_posts_pros_cons_nonce' );
	$post_pros_desc = get_post_meta( $post->ID, 'post_pros_desc', true );
	$post_cons_desc = get_post_meta( $post->ID, 'post_cons_desc', true );
	$post_pros_cons_allowed_html = array(
		'a' => array(
			'href' => true,
			'title' => true,
			'target' => true,
			'rel' => true
		),
		'br' => array(),
		'em' => array(),
		'strong' => array(),
		'span' => array(),
		'p' => array(),
		'ul' => array(),
		'ol' => array(),
		'li' => array(),
	);
?>

<div class="components-base-control post_pros_desc">
	<div class="components-base-control__field">
		<label class="components-base-control__label" for="post_pros_desc-0">
			<?php
				$posts_pros_title = get_option( 'posts_pros_title' );
				if ( $posts_pros_title ) {
					echo esc_html($posts_pros_title);
				} else {
					esc_html_e( 'Pros', 'pluto' );
				}
			?>
		</label>
		<textarea class="components-textarea-control__input" id="post_pros_desc-0" rows="8" name="post_pros_desc" style="display: block; margin-bottom: 10px; width:100%;"><?php echo wp_kses($post_pros_desc, $post_pros_cons_allowed_html); ?></textarea>
	</div>
</div>

<div class="components-base-control post_cons_desc">
	<div class="components-base-control__field">
		<label class="components-base-control__label" for="post_cons_desc-0">
			<?php
				$posts_cons_title = get_option( 'posts_cons_title' );
				if ( $posts_cons_title ) {
					echo esc_html($posts_cons_title);
				} else {
					esc_html_e( 'Cons', 'pluto' );
				}
			?>
		</label>
		<textarea class="components-textarea-control__input" id="post_cons_desc-0" rows="8" name="post_cons_desc" style="display: block; margin-bottom: 10px; width:100%;"><?php echo wp_kses($post_cons_desc, $post_pros_cons_allowed_html); ?></textarea>
	</div>
</div>

    <?php
}

add_action( 'save_post', 'pluto_posts_pros_cons_save_fields', 10, 2 );

function pluto_posts_pros_cons_save_fields( $post_id ) {

		if ( ! isset( $_POST['pluto_posts_pros_cons_nonce'] ) ) {
            return $post_id;
        }

        $nonce = $_POST['pluto_posts_pros_cons_nonce'];

        if ( ! wp_verify_nonce( $nonce, 'pluto_posts_pros_cons_box' ) ) {
            return $post_id;
        }

        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return $post_id;
        }

        $slug = "post";
        if($slug != $post->post_type) {
            if ( ! current_user_can( 'edit_page', $post_id ) ) {
                return $post_id;
            }
        }

		$post_pros_desc = $_POST['post_pros_desc'];
        update_post_meta( $post_id, 'post_pros_desc', $post_pros_desc );

        $post_cons_desc = $_POST['post_cons_desc'];
        update_post_meta( $post_id, 'post_cons_desc', $post_cons_desc );
}

/** PROS AND CONS END */

/* Start of Post Short Code - I */
function posts_shortcode_1($atts) {

	ob_start();

	// Define attributes and their defaults

	extract( shortcode_atts( array (
		'items_number' => 4,
	    'category' => '',
	    'title' => ''
	), $atts ) );

	$first_args = array(
		'posts_per_page'      => 1,
		'cat'                 => $category,
		'no_found_rows'       => true,
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true
	);

	$second_args = array(
		'posts_per_page'      => 2,
		'offset'	          => 1,
		'cat'                 => $category,
		'no_found_rows'       => true,
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true
	);
	$third_args = array(
		'posts_per_page'      => 1,
		'offset'	          => 6,
		'cat'                 => $category,
		'no_found_rows'       => true,
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true
	);

	$post_query_first = new WP_Query( $first_args );
	if ( $post_query_first->have_posts() ) {
        $path =  WP_PLUGIN_DIR . '/post-meta-shortcode/posts/post-sc-1.php';
        include $path;
        

        wp_reset_postdata();
        $posts_items = ob_get_clean();
        return $posts_items;
	}

}
 
add_shortcode('pluto-post-sc-1', 'posts_shortcode_1');
/* End of Post Short Code - I */

/* Start of Post Short Code - II */
function posts_shortcode_2($atts) {

	ob_start();

	// Define attributes and their defaults

	extract( shortcode_atts( array (
		'items_number' => 4,
	    'category' => '',
	    'title' => ''
	), $atts ) );

	$first_args = array(
		'posts_per_page'      => 1,
		'cat'                 => $category,
		'no_found_rows'       => true,
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true
	);

	$second_args = array(
		'posts_per_page'      => 2,
		'offset'	          => 1,
		'cat'                 => $category,
		'no_found_rows'       => true,
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true
	);
	$third_args = array(
		'posts_per_page'      => 1,
		'offset'	          => 6,
		'cat'                 => $category,
		'no_found_rows'       => true,
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true
	);

	$post_query_first = new WP_Query( $first_args );
	if ( $post_query_first->have_posts() ) {
        $path =  WP_PLUGIN_DIR . '/post-meta-shortcode/posts/post-sc-2.php';
        include $path;
        

        wp_reset_postdata();
        $posts_items = ob_get_clean();
        return $posts_items;
	}

}
 
add_shortcode('pluto-post-sc-2', 'posts_shortcode_2');
/* End of Post Short Code - II */

/* Start of Post Short Code - III */
function posts_shortcode_3($atts) {

	ob_start();

	// Define attributes and their defaults

	extract( shortcode_atts( array (
		'items_number' => 4,
	    'category' => '',
	    'title' => ''
	), $atts ) );

	$first_args = array(
		'posts_per_page'      => 1,
		'cat'                 => $category,
		'no_found_rows'       => true,
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true
	);

	$second_args = array(
		'posts_per_page'      => 1,
		'offset'	          => 1,
		'cat'                 => $category,
		'no_found_rows'       => true,
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true
	);
	$third_args = array(
		'posts_per_page'      => 1,
		'offset'	          => 6,
		'cat'                 => $category,
		'no_found_rows'       => true,
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true
	);

	$post_query_first = new WP_Query( $first_args );
	if ( $post_query_first->have_posts() ) {
        $path =  WP_PLUGIN_DIR . '/post-meta-shortcode/posts/post-sc-3.php';
        include $path;
        

        wp_reset_postdata();
        $posts_items = ob_get_clean();
        return $posts_items;
	}

}
 
add_shortcode('pluto-post-sc-3', 'posts_shortcode_3');
/* End of Post Short Code - III */

/* Start of Post Short Code - IV */
function posts_shortcode_4($atts) {

	ob_start();

	// Define attributes and their defaults

	extract( shortcode_atts( array (
		'items_number' => 2,
	    'category' => '',
	    'title' => ''
	), $atts ) );

	$first_args = array(
		'posts_per_page'      => 1,
		'cat'                 => $category,
		'no_found_rows'       => true,
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true
	);

	$second_args = array(
		'posts_per_page'      => 1,
		'offset'	          => 1,
		'cat'                 => $category,
		'no_found_rows'       => true,
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true
	);
	$third_args = array(
		'posts_per_page'      => 3,
		'offset'	          => 6,
		'cat'                 => $category,
		'no_found_rows'       => true,
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true
	);

	$post_query_first = new WP_Query( $first_args );
	if ( $post_query_first->have_posts() ) {
        $path =  WP_PLUGIN_DIR . '/post-meta-shortcode/posts/post-sc-4.php';
        include $path;
        

        wp_reset_postdata();
        $posts_items = ob_get_clean();
        return $posts_items;
	}

}
 
add_shortcode('pluto-post-sc-4', 'posts_shortcode_4');
/* End of Post Short Code - IV */

/* Start of Post Short Code - V */
function posts_shortcode_5($atts) {

	ob_start();

	// Define attributes and their defaults

	extract( shortcode_atts( array (
		'items_number' => 2,
	    'category' => '',
	    'title' => ''
	), $atts ) );

	$first_args = array(
		'posts_per_page'      => 1,
		'cat'                 => $category,
		'no_found_rows'       => true,
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true
	);

	$second_args = array(
		'posts_per_page'      => 1,
		'offset'	          => 1,
		'cat'                 => $category,
		'no_found_rows'       => true,
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true
	);
	$third_args = array(
		'posts_per_page'      => 3,
		'offset'	          => 6,
		'cat'                 => $category,
		'no_found_rows'       => true,
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true
	);

	$post_query_first = new WP_Query( $first_args );
	if ( $post_query_first->have_posts() ) {
        $path =  WP_PLUGIN_DIR . '/post-meta-shortcode/posts/post-sc-5.php';
        include $path;
        

        wp_reset_postdata();
        $posts_items = ob_get_clean();
        return $posts_items;
	}

}
 
add_shortcode('pluto-post-sc-5', 'posts_shortcode_5');
/* End of Post Short Code - V */

/* Start of Post Short Code - VI */
function posts_shortcode_6($atts) {

	ob_start();

	// Define attributes and their defaults

	extract( shortcode_atts( array (
		'items_number' => 2,
	    'category' => '',
	    'title' => ''
	), $atts ) );

	$first_args = array(
		'posts_per_page'      => 1,
		'cat'                 => $category,
		'no_found_rows'       => true,
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true
	);

	$post_query_first = new WP_Query( $first_args );
	if ( $post_query_first->have_posts() ) {
        $path =  WP_PLUGIN_DIR . '/post-meta-shortcode/posts/post-sc-6.php';
        include $path;
        

        wp_reset_postdata();
        $posts_items = ob_get_clean();
        return $posts_items;
	}

}
 
add_shortcode('pluto-post-sc-6', 'posts_shortcode_6');
/* End of Post Short Code - VI */

/* Start of Post Short Code - VII */
function posts_shortcode_7($atts) {

	ob_start();

	// Define attributes and their defaults

	extract( shortcode_atts( array (
		'items_number' => 2,
	    'category' => '',
	    'title' => ''
	), $atts ) );

	$first_args = array(
		'posts_per_page'      => 1,
		'cat'                 => $category,
		'no_found_rows'       => true,
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true
	);

	$post_query_first = new WP_Query( $first_args );
	if ( $post_query_first->have_posts() ) {
        $path =  WP_PLUGIN_DIR . '/post-meta-shortcode/posts/post-sc-7.php';
        include $path;
        

        wp_reset_postdata();
        $posts_items = ob_get_clean();
        return $posts_items;
	}

}
 
add_shortcode('pluto-post-sc-7', 'posts_shortcode_7');
/* End of Post Short Code - VII */


/* Start of Post Short Code - VIII */
function posts_shortcode_8($atts) {

	ob_start();

	// Define attributes and their defaults

	extract( shortcode_atts( array (
		'items_number' => 2,
	    'category' => '',
	    'title' => ''
	), $atts ) );

	$first_args = array(
		'posts_per_page'      => 1,
		'cat'                 => $category,
		'no_found_rows'       => true,
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true
	);

	$post_query_first = new WP_Query( $first_args );
	if ( $post_query_first->have_posts() ) {
        $path =  WP_PLUGIN_DIR . '/post-meta-shortcode/posts/post-sc-8.php';
        include $path;
        

        wp_reset_postdata();
        $posts_items = ob_get_clean();
        return $posts_items;
	}

}
 
add_shortcode('pluto-post-sc-8', 'posts_shortcode_8');
/* End of Post Short Code - VIII */


/* Start of Post Short Code - IX */
function posts_shortcode_9($atts) {

	ob_start();

	// Define attributes and their defaults

	extract( shortcode_atts( array (
		'items_number' => 2,
	    'category' => '',
	    'title' => ''
	), $atts ) );

	$first_args = array(
		'posts_per_page'      => 1,
		'cat'                 => $category,
		'no_found_rows'       => true,
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true
	);

	$post_query_first = new WP_Query( $first_args );
	if ( $post_query_first->have_posts() ) {
        $path =  WP_PLUGIN_DIR . '/post-meta-shortcode/posts/post-sc-9.php';
        include $path;
        

        wp_reset_postdata();
        $posts_items = ob_get_clean();
        return $posts_items;
	}

}
 
add_shortcode('pluto-post-sc-9', 'posts_shortcode_9');
/* End of Post Short Code - IX */

/* Start of Post Short Code - X */
function posts_shortcode_10($atts) {

	ob_start();

	// Define attributes and their defaults

	extract( shortcode_atts( array (
		'items_number' => 2,
	    'category' => '',
	    'title' => ''
	), $atts ) );

	$first_args = array(
		'posts_per_page'      => 1,
		'cat'                 => $category,
		'no_found_rows'       => true,
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true
	);

    $second_args = array(
		'posts_per_page'      => 1,
		'offset'	          => 1,
		'cat'                 => $category,
		'no_found_rows'       => true,
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true
	);
	$third_args = array(
		'posts_per_page'      => 3,
		'offset'	          => 6,
		'cat'                 => $category,
		'no_found_rows'       => true,
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true
	);


	$post_query_first = new WP_Query( $first_args );
	if ( $post_query_first->have_posts() ) {
        $path =  WP_PLUGIN_DIR . '/post-meta-shortcode/posts/post-sc-10.php';
        include $path;
        

        wp_reset_postdata();
        $posts_items = ob_get_clean();
        return $posts_items;
	}

}
 
add_shortcode('pluto-post-sc-10', 'posts_shortcode_10');
/* End of Post Short Code - X */

/* Start of Post Short Code - XI */
function posts_shortcode_11($atts) {

	ob_start();

	// Define attributes and their defaults

	extract( shortcode_atts( array (
		'items_number' => 2,
	    'category' => '',
	    'title' => ''
	), $atts ) );

	$first_args = array(
		'posts_per_page'      => 1,
		'cat'                 => $category,
		'no_found_rows'       => true,
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true
	);

    $second_args = array(
		'posts_per_page'      => 1,
		'offset'	          => 1,
		'cat'                 => $category,
		'no_found_rows'       => true,
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true
	);
	$third_args = array(
		'posts_per_page'      => 3,
		'offset'	          => 6,
		'cat'                 => $category,
		'no_found_rows'       => true,
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true
	);


	$post_query_first = new WP_Query( $first_args );
	if ( $post_query_first->have_posts() ) {
        $path =  WP_PLUGIN_DIR . '/post-meta-shortcode/posts/post-sc-11.php';
        include $path;
        

        wp_reset_postdata();
        $posts_items = ob_get_clean();
        return $posts_items;
	}

}
 
add_shortcode('pluto-post-sc-11', 'posts_shortcode_11');
/* End of Post Short Code - XI */

/* Start of Post Short Code - XII */
function posts_shortcode_12($atts) {

	ob_start();

	// Define attributes and their defaults

	extract( shortcode_atts( array (
		'items_number' => 2,
	    'category' => '',
	    'title' => ''
	), $atts ) );

	$first_args = array(
		'posts_per_page'      => 1,
		'cat'                 => $category,
		'no_found_rows'       => true,
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true
	);

    $second_args = array(
		'posts_per_page'      => 1,
		'offset'	          => 1,
		'cat'                 => $category,
		'no_found_rows'       => true,
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true
	);
	$third_args = array(
		'posts_per_page'      => 3,
		'offset'	          => 6,
		'cat'                 => $category,
		'no_found_rows'       => true,
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true
	);


	$post_query_first = new WP_Query( $first_args );
	if ( $post_query_first->have_posts() ) {
        $path =  WP_PLUGIN_DIR . '/post-meta-shortcode/posts/post-sc-12.php';
        include $path;
        

        wp_reset_postdata();
        $posts_items = ob_get_clean();
        return $posts_items;
	}

}
 
add_shortcode('pluto-post-sc-12', 'posts_shortcode_12');
/* End of Post Short Code - XI */

